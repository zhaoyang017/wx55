window.onload = function(){
    init();
}
function init(){
    window.onscroll = function(){
        var t =document.documentElement.scrollTop||document.body.scrollTop;
        if(t > 200){
            $('.btn_goTop').show();
        }else if(t < 50){
            $('.btn_goTop').hide();
        }
    }
    $('.btn_goTop').click(function(){
        $('body,html').animate({scrollTop:0},1000)
    })
}