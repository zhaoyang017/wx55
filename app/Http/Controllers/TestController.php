<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Mockery\Exception;
use Intervention\Image\Facades\Image;

class TestController extends Controller
{
    public function index()
    {
        $accessKey = 'LTAIa9aYQR9U9UlS';
        $accessSecret = 'CTSiORGIQvTJpnYHgmXfUqxbDkvKJy';
//        $nonce = sprintf('%d000%d', time(), rand(1000, 9999));
        $nonce = '9966';
        $timestamp = Carbon::now('GMT')
            ->format('D, d M Y H:i:s T');
        $content = '6666';
        $body = [
            'scenes' => ['antispam'],
            'tasks' => [
                'content' => $content,
            ]
        ];
        $header = 'x-acs-signature-method:HMAC-SHA1\nx-acs-signature-nonce:' . $nonce
            . '\nx-acs-signature-version:1.0\nx-acs-version:2017-01-12\n';
        $bytes = '/green/text/scan';
        $bytes = urlencode($bytes);
        $content_md5 = base64_encode(md5(json_encode($body), true));
        $authorization = 'POST\n' . 'application/json\n' . $content_md5 . '\n' . 'application/json\n' . $timestamp . '\n'
            . $header . $bytes;
        $authorization = hash_hmac('sha256', $authorization, $accessSecret, true);
        $authorization = base64_encode($authorization);
        $authorization = 'acs ' . $accessKey . ':' . $authorization;

        $client = new Client([
            'base_uri' => 'http://green.cn-shanghai.aliyuncs.com',
            'timeout' => 2.0,
        ]);
        $res = $client->request('POST', '/green/text/scan', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Content-MD5' => $content_md5,
                'Date' => $timestamp,
                'x-acs-signature-method' => 'HMAC-SHA256',
                'x-acs-signature-nonce' => $nonce,
                'x-acs-signature-version' => '1.0',
                'x-acs-version' => '2017-01-12',
                'Authorization' => $authorization
            ],
            'body' => json_encode($body),
            'debug' => true
        ]);
    }

    public function test2()
    {
        $messageStruct = '12312415';
        $messageId = rand(1, 100000);
        $postIp = '127.0.0.1';
        $accountType = 1;
        $uid = '1252946101';
        $appId = '1252946101';

        $SecretKey = 'JWXP9jsz8JaxFLFx46xpX3QbaR8dw2we';
        $method = 'GET';
        $url = 'csec.api.qcloud.com/v2/index.php';
        $Action = 'UgcAntiSpam';
        $Nonce = '23123';
        $Region = 'ap-shanghai-2';
        $SecretId = 'AKIDHjCXhfY3ShkcImzA3kFalmcIm4C8AlY9';
        $Timestamp = time();

        $bytes = $method . $url . '?Action=' . $Action . '&Nonce=' . $Nonce . '&Region=' . $Region . '&SecretId=' . $SecretId . '&Timestamp=' . $Timestamp;
        $Signature = base64_encode(hash_hmac('sha1', $bytes, $SecretKey, true));
        $Signature = urlencode($Signature);

        $client = new Client([
            'timeout' => 2.0,
        ]);
        $urlStr = 'Action=' . $Action . '&Nonce=' . $Nonce . '&Region=' . $Region . '&SecretId=' . $SecretId . '' . '&Timestamp='
            . $Timestamp . '&Signature' . $Signature . '&messageStruct='
            . $messageStruct . '&messageId=' . $messageId . '&postIp=' . $postIp . '&accountType=' . $accountType . '&uid=' . $uid . '&appId=' . $appId;
//        dd('https://csec.api.qcloud.com/v2/index.php?'. $urlStr);
        $res = $client->request('GET', 'https://csec.api.qcloud.com/v2/index.php?' . $urlStr, [
        ]);
        return $res->getBody();
    }

    public function test3()
    {
        $app_id = '1106407667';
        $time_stamp = time();
        $nonce_str = rand(1, 100000);
        $model = 1;
        $app_key = '4DmVJXJdPy85aid7';
        $image = Image::make('face.jpeg')->encode('data-url');
        $image = str_replace('data:image/jpeg;base64,', '' , $image);
//        dd($image);
        $str = 'app_id='.urlencode($app_id) . '&image=' . urlencode($image)
            . '&model=' .urlencode($model) . '&nonce_str=' . urlencode($nonce_str)
            . '&time_stamp=' . urlencode($time_stamp) . '&app_key=' . urlencode($app_key);
//        dd($str);
        $sign = strtoupper(md5($str));
        $client = new Client([
            'timeout' => 10.0,
        ]);
        $body = 'app_id='.urlencode($app_id)
            . '&model=' .urlencode($model) . '&nonce_str=' . urlencode($nonce_str)
            . '&time_stamp=' . urlencode($time_stamp) . '&sign=' . $sign . '&image=' . urlencode($image);
//        dd($body);
        $res = $client->request('post', 'https://api.ai.qq.com/fcgi-bin/ptu/ptu_facemerge', [
            'form_params' => [
                'app_id' => $app_id,
                'model' => $model,
                'nonce_str' => $nonce_str,
                'time_stamp' => $time_stamp,
                'sign' => $sign,
                'image' => $image
            ],
        ]);
        $json = $res->getBody();
        return $res->getBody();
    }
}
