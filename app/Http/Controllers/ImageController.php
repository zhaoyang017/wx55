<?php

namespace App\Http\Controllers;

use App\Models\Coord;
use App\Models\Vvip;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    public function index()
    {
        $coords = Coord::all();

        $bg = Image::make(public_path('res/img/castrol-bg.png'));
        $img = Image::make(public_path('res/img/photo.png'))->resize(16, 16);
        foreach ($coords as $coord) {
            $bg = $bg->insert($img, 'top-left', $coord->x, $coord->y);
        }
        return $bg->response();
    }

    public function data()
    {
        for ($i = 0; $i < 18; $i++) {
            $vip = new Vvip();
            $rnd = 'B' . substr(time() * mt_rand(1111, 9999) * mt_rand(1, 9), -7, 7);
            $vip->fill(['code' => $rnd]);
            $vip->save();
        }
        return 'true';
    }
}
